const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

//mongoose.connect("mongodb://localhost:27017/databaseName")  <robo 3t connection

mongoose.connect("mongodb+srv://mervinmani:Ch3rrylyn@cluster0.hr2ax.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser : true,
			useUnifiedTopology: true
		}
)
let db = mongoose.connection;

//connection error message
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))



app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.listen(port, ()=> console.log(`Server running at port ${port}`));


//mongoose schemas
//blueprints model
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}	
})

const Task = mongoose.model("Task", taskSchema);

//routes/endpoints

//create a new task


//Business Logic
/*
1.
*/

app.post("/tasks", (req, res) =>{
	Task.findOne({name: req.body.name}, (err, result) => {
	if(result != null && result.name == req.body.name){
		return res.send("Duplicate task found");
	} else{
		let newTask = new Task({
			name: req.body.name
		});

		newTask.save((saveErr, savedTask) => {
			if(saveErr){
				return console.error(saveErr)
			}else{
				return res.status(201).send("New Task Created")
			}
		})


	}	
  })
})


//Get all tasks
//Business logic
/*
*/
app.get("/tasks", (req, res) =>{
	Task.find({}, (err, result) =>{
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}

	})
})

//===================ACTIVITY=====================//
//User schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

//user model
const User = mongoose.model("User", userSchema);

//signup
app.post("/signup", (req, res) =>{
	User.findOne({username: req.body.username}, (err, result) => {
	if(result != null && result.username == req.body.username){
		return res.send("Already Registered");
	} else{		
		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		});

		newUser.save((saveErr, savedTask) => {
			if(saveErr){
				return console.error(saveErr)
			}else{
				return res.status(201).send("Registration Success")
			}
		})


	}	
  })
})

//get all users
app.get("/users", (req, res) =>{
	User.find({}, (err, result) =>{
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}

	})
})

